pandas==3.10
sshtunnel==0.4.0
sqlalchemy==1.4.41
SQLAlchemy-Utils==0.38.3
psycopg2==2.9.5
matplotlib==3.6.0